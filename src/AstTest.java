import static org.junit.Assert.*;

import org.junit.Test;

public class AstTest {
  @Test
  public void atomTest() {
    assertEquals("a", new Atom("a").toString());
  }

  @Test
  public void emptyListTest() {
    assertEquals("()", new Nil().toString());
  }

  @Test
  public void listTest() {
    assertEquals("(a)", new Cons(new Atom("a"), new Nil()).toString());
  }

  @Test
  public void nestedListTest() {
    assertEquals(
        "((a))",
        new Cons(new Cons(new Atom("a"), new Nil()), new Nil()).toString());
  }

  @Test
  public void multiItemListTest() {
    assertEquals(
        "(a b)",
        new Cons(new Atom("a"), new Cons(new Atom("b"), new Nil())).toString());
  }

  @Test
  public void complexListTest() {
    assertEquals(
        "((a) (b))",
        new Cons(new Cons(new Atom("a"), new Nil()),
                 new Cons(new Cons(new Atom("b"), new Nil()), new Nil()))
            .toString());
  }
}
