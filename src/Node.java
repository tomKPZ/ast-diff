
public abstract class Node {
  public abstract StringBuilder toStringBuilder(boolean is_left_child);

  public String toString() { return toStringBuilder(true).toString(); }
}
