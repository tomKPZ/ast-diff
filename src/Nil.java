
public class Nil extends List {
  @Override
  public StringBuilder toStringBuilder(boolean is_left_child) {
    if (is_left_child) {
      return new StringBuilder("()");
    }
    return new StringBuilder("");
  }
}
