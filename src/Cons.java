
public class Cons extends List {
  private Node head;
  private List rest;

  public Cons(Node head, List rest) {
    this.head = head;
    this.rest = rest;
  }

  @Override
  public StringBuilder toStringBuilder(boolean is_left_child) {
    StringBuilder builder = new StringBuilder();
    if (is_left_child) {
      builder.append("(");
    } else {
      builder.append(" ");
    }
    builder.append(head.toStringBuilder(true));
    builder.append(rest.toStringBuilder(false));
    if (is_left_child) {
      builder.append(")");
    }
    return builder;
  }
}
