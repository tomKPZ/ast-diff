#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
find "${DIR}" -name "*.java" | xargs clang-format -i
