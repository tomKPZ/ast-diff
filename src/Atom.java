
public class Atom extends Node {
  private String value;

  public Atom(String value) { this.value = value; }

  @Override
  public StringBuilder toStringBuilder(boolean is_left_child) {
    return new StringBuilder(value);
  }
}
